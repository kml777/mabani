from copy import deepcopy

class cell:
	def __init__(self,kind=None,color=0):
		self.kind=kind
		self.color=color

board=[[cell() for i in range(0,8)] for j in range(0,8)]


#the first line starts at 0 0 and ends at 0 7 be careful!!!
class king:
	val=int(1e9)

	def __init__(self,x,y,color):
		self.x=x
		self.y=y
		self.color=color

	def next_move(self,pos=board):
		x=self.x
		y=self.y
		for i in range(-1,2):
			for j in range(-1,2):
				if(i or j):
					res=deepcopy(pos)
					if(x+i<8 and y+j<8 and x+i>=0 and y+j>=0 and pos[x+i][y+j].color!=self.color):
						res[x+i][y+j].color=self.color
						res[x+i][y+j].kind="king"
						res[x][y].color=self.color
						res[x][y].kind=None
						yield res

class queen:
	val=9
	def __init__(self,x,y,color):
		self.x=x
		self.y=y
		self.color=color
	def next_move(self,pos=board):
		x=self.x
		y=self.y
		for i in range(x+1,8):
			if(pos[i][y].color==self.color):
				break
			elif(pos[i][y].color):
				res=deepcopy(pos)
				res[i][y].kind="queen"
				res[i][y].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[i][y].kind = "queen"
				res[i][y].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(x-1,-1,-1):
			if(pos[i][y].color==self.color):
				break
			elif(pos[i][y].color):
				res=deepcopy(pos)
				res[i][y].kind="queen"
				res[i][y].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res=deepcopy(pos)
				res[i][y].kind = "queen"
				res[i][y].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(y+1,8):
			if(pos[x][i].color==self.color):
				break
			elif(pos[x][i].color):
				res=deepcopy(pos)
				res[x][i].kind="queen"
				res[x][i].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[x][i].kind = "queen"
				res[x][i].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(y-1,-1,-1):
			if(pos[x][i].color==self.color):
				break
			elif(pos[x][i].color):
				res=deepcopy(pos)
				res[x][i].kind="queen"
				res[x][i].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[x][i].kind = "queen"
				res[x][i].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		px,py=x,y

		while(px<8 and py<8):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="queen"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "queen"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px+=1
			py+=1
		while(px>=0 and py<8):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="queen"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "queen"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px-=1
			py+=1
		while(px<8 and py>=0):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="queen"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "queen"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px+=1
			py-=1
		while(px>=0 and py>=0):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="queen"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "queen"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px-=1
			py-=1


class rook:
	val=5
	def __init__(self,x,y,color):
		self.x=x
		self.y=y
		self.color=color
	def next_move(self,pos=board):
		x=self.x
		y=self.y
		for i in range(x+1,8):
			if(pos[i][y].color==self.color):
				break
			elif(pos[i][y].color):
				res=deepcopy(pos)
				res[i][y].kind="rook"
				res[i][y].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[i][y].kind = "rook"
				res[i][y].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(x-1,-1,-1):
			if(pos[i][y].color==self.color):
				break
			elif(pos[i][y].color):
				res=deepcopy(pos)
				res[i][y].kind="rook"
				res[i][y].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[i][y].kind = "rook"
				res[i][y].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(y+1,8):
			if(pos[x][i].color==self.color):
				break
			elif(pos[x][i].color):
				res=deepcopy(pos)
				res[x][i].kind="rook"
				res[x][i].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[x][i].kind = "rook"
				res[x][i].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res
		for i in range(y-1,-1,-1):
			if(pos[x][i].color==self.color):
				break
			elif(pos[x][i].color):
				res=deepcopy(pos)
				res[x][i].kind="rook"
				res[x][i].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
				break
			else:
				res = deepcopy(pos)
				res[x][i].kind = "rook"
				res[x][i].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield res


class bishop:
	val=3
	def __init__(self,x,y,color):
		self.x=x
		self.y=y
		self.color=color
	def next_move(self,pos=board):
		x=self.x
		y=self.y
		px,py=x,y
		while(px<8 and py<8):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="bishop"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "bishop"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px+=1
			py+=1
		while(px>=0 and py<8):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="bishop"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "bishop"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px-=1
			py+=1
		while(px<8 and py>=0):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="bishop"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "bishop"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px+=1
			py-=1
		while(px>=0 and py>=0):
			res = deepcopy(pos)
			if(pos[px][py].color==self.color):
				break
			elif(pos[px][py].color):
				res[px][py].kind="bishop"
				res[px][py].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield
				break
			else:
				res[px][py].kind = "bishop"
				res[px][py].color = self.color
				res[x][y].kind = None
				res[x][y].color = 0
				yield
			px-=1
			py-=1

class knight:
	val=3
	def __init__(self,x,y,color):
		self.x=x
		self.y=y
		self.color=color
	def next_move(self,pos=board):
		x=self.x
		y=self.y
		for i in [-1,1,-2,2]:
			for j in [-1,1,-2,2]:
				if(abs(i)!=abs(j)):
					res=deepcopy(pos)
					if(x+i<8 and y+j<8 and x+i>=0 and y+j>=0 and res[x+i][y+j].color!=self.color):
						res[x+i][y+j].color=self.color
						res[x+i][y+j].kind="knight"
						res[x][y].color=0
						res[x][y].kind=None
						yield res

class pawn:
	val=1
	def __init__(self,x,y,color,cnt=0):
		self.x=x
		self.y=y
		self.color=color
		self.cnt=cnt
	def next_move(self,pos=board):
		x=self.x
		y=self.y
		res=deepcopy(pos)
		if(not self.cnt):
			y1=y-2
			if(self.color==1):
				y1=y+2
			if(x<8 and x>=0 and y1<8 and y1>=0 and res[x][y1].color==0):
					res[x][y1].kind="pawn"
					res[x][y1].color=self.color
					res[x][y].kind=None
					res[x][y].color=0
					yield res
		y1=y+1
		if(self.color==2):
			y1=y-1
		if(x<8 and x>=0 and y1<8 and y1>=0 and res[x][y1].color==0):
				res[x][y1].kind="pawn"
				res[x][y1].color=self.color
				res[x][y].kind=None
				res[x][y].color=0
				yield res
		x1,x2=x-1,x+1
		if(x1>=0 and x1<8 and y1<8 and y1>=0and res[x1][y1].color==1-(1-self.color)+1):
			res[x1][y1].kind = "pawn"
			res[x1][y1].color = self.color
			res[x][y].kind = None
			res[x][y].color = 0
			yield res
		if(x2<8 and x2>=0 and y1<8 and y1>=0 and res[x2][y1].color==1-(1-self.color)+1):
			res[x2][y1].kind = "pawn"
			res[x2][y1].color = self.color
			res[x][y].kind = None
			res[x][y].color = 0
			yield res

def makepiece(pos):
	res=[]
	for i in range(0,8):
		for j in range(0,8):
			if(pos[i][j].kind=="king"):
				res.append(king(i,j,pos[i][j].color))
			elif(pos[i][j].kind=="queen"):
				res.append(queen(i,j,pos[i][j].color))
			elif(pos[i][j].kind=="rook"):
				res.append(rook(i,j,pos[i][j].color))
			elif(pos[i][j].kind=="knight"):
				res.append(knight(i,j,pos[i][j].color))
			elif(pos[i][j].kind=="bishop"):
				res.append(bishop(i,j,pos[i][j].color))
			elif(pos[i][j].kind=="pawn"):
				p=pawn(i,j,pos[i][j].color)
				if(pos[i][j].color==2 and i!=6)or (pos[i][j].color==1 and i!=1):
					p.cnt=1
				res.append(p)
	return res


def check(pos,color):
	x,y=-1,-1
	for i in range(0,8):
		for j in range(0,8):
			if(pos[i][j].kind=="king" and pos[i][j].color==color):
				x,y=i,j
	x1,y1=x+1,y
	while(x1<8):
		if((pos[x1][y]=="queen" or pos[x1][y]=="rook" )and(pos[x1][y].color==1-(color-1)+1)):
			return True
		elif(pos[x1][y].color==color):
			break
		x1+=1
	x1=x-1
	while(x1>=0):
		if ((pos[x1][y] == "queen" or pos[x1][y] == "rook") and (pos[x1][y].color == 1 - (color - 1) + 1)):
			return True
		elif (pos[x1][y].color == color):
			break
		x1 -= 1
	y1=y1+1
	while(y1<8):
		if((pos[x][y1]=="queen" or pos[x][y1]=="rook" )and(pos[x][y1].color==1-(color-1)+1)):
			return True
		elif(pos[x][y1].color==color):
			break
		y1+=1
	y1=y-1
	while(y1>=0):
		if ((pos[x][y1] == "queen" or pos[x][y1] == "rook") and (pos[x][y1].color == 1 - (color - 1) + 1)):
			return True
		elif (pos[x][y1].color == color):
			break
		y1 -= 1
#rooks and queen
	x1,y1=x+1,y+1
	while(x1<8 and y1 <8):
		if((pos[x1][y1]=="queen" or pos[x1][y1]=="rook" )and(pos[x1][y1].color==1-(color-1)+1)):
			return True
		elif(pos[x1][y1].color==color):
			break
		x1+=1
		y1+=1
	x1,y1=x-1,y-1
	while(x1>=0 and y1>=0):
		if ((pos[x1][y1] == "queen" or pos[x1][y1] == "rook") and (pos[x1][y1].color == 1 - (color - 1) + 1)):
			return True
		elif (pos[x1][y1].color == color):
			break
		x1 -= 1
		y1-=1
	x1,y1=x+1,y-1
	while(y1>=0 and x1<8):
		if((pos[x1][y1]=="queen" or pos[x1][y1]=="rook" )and(pos[x1][y1].color==1-(color-1)+1)):
			return True
		elif(pos[x1][y1].color==color):
			break
		y1-=1
		x1+=1
	x1,y1=x-1,y+1
	while(y1<8 and x1>=0):
		if ((pos[x1][y1] == "queen" or pos[x1][y1] == "rook") and (pos[x1][y1].color == 1 - (color - 1) + 1)):
			return True
		elif (pos[x1][y1].color == color):
			break
		y1 += 1
		x1-=1
#bishops and queen
	for i in [-1,1,-2,2]:
		for j in [-1,1,-2,2]:
			if(abs(i)!=abs(j)):
				if(x+i<8 and y+j<8 and x+i>=0 and y+j>=0 and pos[x+i][y+j].kind=="king" and pos[x+i][y+j].color==2-(color-1)):
					return True
	return False


def get_val(pos):
	pi=makepiece(pos)
	res=0
	for i in pi:
		if(i.color==1):
			res-=i.val
		elif(i.color==2):
			res+=i.val
	return res

def ai(depth=0,color=2,pos=board):
	if(depth==0 and color==2):
		print("in AI pos:\n")
		for i in range(0, 8):
			for j in range(0, 8):
				print(pos[i][j].kind, end=" ")
			print()
	pi=makepiece(pos)
	score=int(1e9)
	q=deepcopy(pos)
	flag=0
	if(color==2):
		score*=-1
	for i in pi:
		if(color==i.color):
			for j in i.next_move(pos):
				if(not check(j,color)):
					flag=1
					if(depth==3):
						if(color==2 and score<=get_val(j)):
							score,q=get_val(j),deepcopy(j)
						elif(color==1 and score>=get_val(j)):
							score,q=get_val(j),deepcopy(j)
					else:
						u,v=ai(depth+1,1-(color-1)+1,j)
						if(color==2 and score<=v):
							score,q=v,deepcopy(j)
						elif(color==1 and score>=v):
							score,q =v,deepcopy(j)
	return (q,score)

def makestring(pos):
	res=""

	for i in range(0,8):
		for j in range(0,8):
			kind=pos[i][j].kind
			col=pos[i][j].color
			if(kind=="pawn" and col==1):
				res+='0'
			elif(kind=="pawn" and col==2):
				res+='1'
			elif(kind=="rook" and col==1):
				res+='2'
			elif(kind=="rook" and col==2):
				res+='3'
			elif(kind=="bishop" and col==1):
				res+='4'
			elif(kind=="bishop" and col==2):
				res+='5'
			elif(kind=="king" and col==1):
				res+='6'
			elif(kind=="king" and col==2):
				res+='7'
			elif(kind=="knight" and col==1):
				res+='8'
			elif(kind=="knight" and col==2):
				res+='9'
			elif(kind=="queen" and col==1):
				res+='a'
			elif(kind=="queen" and col==2):
				res+='b'
			else :
				res+='.'

	return res

def init():
	for i in range (0,8):
		for j in range(0,8):
			board[i][j].kind=None
			board[i][j].color=0
	board[0][0].kind=board[0][7].kind=board[7][0].kind=board[7][7].kind="rook"
	board[0][1].kind= board[0][6].kind = board[7][1].kind = board[7][6].kind = "knight"
	board[0][2].kind = board[0][5].kind = board[7][2].kind = board[7][5].kind = "bishop"
	board[0][3].kind=board[7][3].kind="queen"
	board[0][4].kind=board[7][4].kind="king"
	for i in range(0,8):
		board[1][i].kind=board[6][i].kind="pawn"
		board[0][i].color=board[1][i].color=1
		board[7][i].color=board[6][i].color=2

	return makestring(board)

def abbas(move):
	global board
	first,last=move.split()
	x1,y1=int(first[1]),ord(first[0])-ord('a')
	x2, y2 = int(last[1]), ord(last[0]) - ord('a')
	x1-=1
	x2-=1
	kin=board[x1][y1].kind
	col1=board[x1][y1].color
	col2=board[x2][y2].color


	if(col1==col2 or col1!=1 or x1>=8 or x1<0 or x2>=8 or x2<0 or y1>=8 or y1<0 or y2>=8 or y2<0):
		return "invalid"
	if(kin=="king"):
		if(abs(x2-x1)<=1 and abs(y2-y1)<=1):
			board[x2][y2]=board[x1][y1]
			board[x1][y1]=cell()
			q,score=ai(pos=board)
			if(q==board):
				return "you win!"
			p2,score=ai(color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"
	if(kin=="knight"):
		if(abs(x2-x1) in [1,2] and abs(y2-y1) in [1,2] and abs(x1-x2)!=abs(y2-y1)):
			board[x1][y1], board[x2][y2] = cell(), board[x1][y1]
			q, score = ai(pos=board)
			if (q == board):
				return "you win!"
			p2,score=ai(color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"
	if(kin=="bishop"):
		if((x1-y1==x2-y2) or (x1+y1==x2+y2)):
			board[x1][y1], board[x2][y2] = cell(), board[x1][y1]
			q, score = ai(pos=board)
			if (q == board):
				return "you win!"
			p2,score=ai(color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"
	if(kin=="rook"):
		if(x1==x2 or y1==y2):
			board[x1][y1], board[x2][y2] = cell(), board[x1][y1]
			q, score = ai(pos=board)
			if (q == board):
				return "you win!"
			p2,score=ai(color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"
	if(kin=="queen"):
		if((x1 - y1 == x2 - y2) or (x1 + y1 == x2 + y2)) or (x1==x2 or y1==y2):
			board[x1][y1], board[x2][y2] = cell(), board[x1][y1]
			q, score = ai(pos=board)
			if (q == board):
				return "you win!"
			p2,score=ai(color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"

	if(kin=="pawn"):
		if((x1==1 and x2==3) or(x2-x1==1) or (abs(y1-y2)==1 and x2==x1+1 and board[x2][y2].color==2)):
			print("before move:\n")
			for i in range(0,8):
				for j in range(0,8):
					print(board[i][j].kind,end=" ")
				print()
			board[x1][y1], board[x2][y2] = cell(), board[x1][y1]
			print("after move:\n")
			for i in range(0,8):
				for j in range(0,8):
					print(board[i][j].kind,end=" ")
				print()
			q, score = ai(pos=board)
			print("after AI :\n")
			for i in range(0, 8):
				for j in range(0, 8):
					print(q[i][j].kind, end=" ")
				print()
			if (q == board):
				return "you win!"
			p2,score=ai(depth=3,color=1,pos=q)
			if(p2==q):
				return "I win!"
			board=deepcopy(q)
			return makestring(q)
		else:
			return "invalid"