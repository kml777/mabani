from django.conf.urls import url
from . import views

app_name = 'ata'
urlpatterns = [
    url(r'^login/', views.longin, name="login"),
    url(r'^register', views.register, name="register"),
    url(r'logout',views.longout,name="logout"),
    url(r'^activate/(.*)',views.acitvate_user,name="activate"),
    url(r'^game/',views.abbas_agha,name="game"),
]