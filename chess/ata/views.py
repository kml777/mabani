from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth import authenticate, login,logout
from django.core.mail import EmailMessage
from .chess import abbas,init,ai
from pprint import pprint
from ata.models import last_position

def baseconvert(n, base):
    """convert positive decimal integer n to equivalent in another base (2-36)"""

    digits = "0123456789abcdefghijklmnopqrstuvwxyz"
    n = int(n)
    base = int(base)

    s = ""
    while 1:
        r = n % base
        s = digits[r] + s
        n = n // base
        if n == 0:
            break

    return s



def longin(request):
    if(request.method=="POST"):
        username = request.POST.get ('username')
        password = request.POST.get ('password')
        username=username.lower()
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("ata:login", args=()))
        else:
            return HttpResponse("Username not found or is not active.")
            # Return an 'invalid login' error message.
            #TODO
    else:
        return render(request,"ata/login.html")


def register(request):
    if(request.method=='POST'):
        username=request.POST.get ('username')
        username=username.lower()
        password=request.POST.get ('password')
        c_password = request.POST.get ('c_password')
        email=request.POST.get ('email')
        if ( (not User.objects.filter ( username = username ).first ()) and (not User.objects.filter ( email = email ).first ()) ):
            if ( password == c_password ):
                username=username.lower()
                account=User.objects.create_user(username, email = email, password = password)
                account.is_active=False
                account.save()
                pos = last_position(pos=init(),tous = account)
                pos.save()
                account.save()
                suff=int(username,36)
                email = EmailMessage('activation email', '127.0.0.1:8000/activate/%s'%suff, to=(email,))
                email.send()
                return HttpResponse ("An activation email has been sent!")
            else:
                return HttpResponse ("Passwords dont match!")
        else:
            return HttpResponse ("Username or email already taken!")
    else:
        return render(request,"ata/register.html")

def acitvate_user(request,a):
    a=baseconvert(a,36)
    if(User.objects.filter ( username = a ).first ()):
        b=User.objects.filter ( username = a ).first ()
        b.is_active=True
        b.save ()
        return HttpResponse("ok!")
    else:
        return HttpResponse("failed!")


def longout(request):
    logout(request)
    return HttpResponse("logged out!")

def abbas_agha(request):
    if(request.method=="POST"):
        if(request.POST.get('s')=="reset"):
            t=init()
            use=request.user
            x = use.last_position
            x.pos=t
            x.save()
            # use.save()
        else:
            t=abbas(request.POST.get('s'))
        if(t=="invalid"):
            return HttpResponse("invalid move")
        elif(t=="you win!"):
            return  HttpResponse(t)
        elif(t=="I win!"):
            return HttpResponse(t)
        print("\n"*2+"POSTING:")
        s = [t[8 * i:8 * i + 8] for i in range(0, 8)]
        pprint(s)
        use=request.user
        x = use.last_position
        x.pos = t
        x.save()
        # use.last_position.pos=t
        # use.save()
        return render(request, "ata/game.html", {'pos': s, 'kooft': tuple(i for i in range(0, 8))})
    else:
        use=request.user
        t = last_position.objects.get(tous = use).pos

        s=[t[8*i:8*i+8] for i in range(0,8)]
        print("\n" * 2 + "GETTING:")
        pprint(s)
        return render(request,"ata/game.html",{'pos':s,'kooft': tuple(i for i in range(0, 8))})